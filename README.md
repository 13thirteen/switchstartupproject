# SwitchStartupProject

A **Visual Studio extension** that allows you to quickly **select startup projects from a dropdown**.

![Preview](https://heptapod.host/thirteen/switchstartupproject/raw/branch/current/preview.png)

No more scrolling up and down the solution explorer tree just to change the startup project!

## Features

* Select the startup project from a dropdown. (Like you do for the platform or configuration.)
* Switch between multi-project startup configurations. (e.g. [B and C] or [A, C and D])
* Configure command line arguments and other parameters for the startup projects.

## Get it!

* Download the newest [release for Visual Studio 2022](https://marketplace.visualstudio.com/items?itemName=vs-publisher-141975.SwitchStartupProjectForVS2022)
* Download the newest [release for Visual Studio 2019](https://marketplace.visualstudio.com/items?itemName=vs-publisher-141975.SwitchStartupProjectForVS2019)
* Download the newest [release for Visual Studio 2017](https://marketplace.visualstudio.com/items?itemName=vs-publisher-141975.SwitchStartupProjectforVS2017)
* Download the newest [release for Visual Studio 2015, 2013, 2012 or 2010](https://marketplace.visualstudio.com/items?itemName=vs-publisher-141975.SwitchStartupProject)
* Source code on [heptapod.host](https://heptapod.host/thirteen/switchstartupproject/)

## More Information
* [Configuration](https://heptapod.host/thirteen/switchstartupproject/blob/branch/current/Configuration.md)
* [Release Notes](https://heptapod.host/thirteen/switchstartupproject/blob/branch/current/ReleaseNotes.md)
* [License](https://heptapod.host/thirteen/switchstartupproject/blob/branch/current/LICENSE.md)

## Found a problem?

Use the [Issue Tracker](https://heptapod.host/thirteen/switchstartupproject/issues)
